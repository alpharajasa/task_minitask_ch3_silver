package com.example.chapter_3_allminitask.fragment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.BottomNavigation
import com.example.chapter_3_allminitask.R
import com.example.chapter_3_allminitask.databinding.ActivityIniFragmentBinding

class iniActivityFragment : AppCompatActivity() {

    private lateinit var binding : ActivityIniFragmentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIniFragmentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val testFragment = BlankFragment()
        fragmentTransaction.add(R.id.frame_layout, testFragment)
        fragmentTransaction.commit()

        binding.btnToBottomNavigation.setOnClickListener {
            val Intent = Intent(this, BottomNavigation::class.java)
            startActivity(Intent)
        }
    }
}