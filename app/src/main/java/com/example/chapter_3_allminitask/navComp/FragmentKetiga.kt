package com.example.chapter_3_allminitask.navComp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.chapter_3_allminitask.databinding.FragmentKeduaBinding
import com.example.chapter_3_allminitask.databinding.FragmentKetigaBinding

class FragmentKetiga : Fragment() {

    private var _binding: FragmentKetigaBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKetigaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val aName = FragmentKetigaArgs.fromBundle(arguments as Bundle).name
        val aNomor = FragmentKetigaArgs.fromBundle(arguments as Bundle).nomor
        binding.tvName.text = "Nama kamu: $aName"
        binding.tvNomor.text = "Nomor kamu: $aNomor"


    }


}