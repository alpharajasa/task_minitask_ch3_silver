package com.example.chapter_3_allminitask.navComp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivityNavCompBinding

class NavComp : AppCompatActivity() {
    private lateinit var binding : ActivityNavCompBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNavCompBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}