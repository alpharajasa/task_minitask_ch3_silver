package com.example.chapter_3_allminitask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivityAndroidPermissionBinding

class AndroidPermission : AppCompatActivity() {
    private lateinit var binding : ActivityAndroidPermissionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityAndroidPermissionBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)



    }
}