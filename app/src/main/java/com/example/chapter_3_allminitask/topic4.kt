package com.example.chapter_3_allminitask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivityTopic4Binding
import com.example.chapter_3_allminitask.latihan.Latihan
import com.example.chapter_3_allminitask.parceableFile.Parceable
import com.example.chapter_3_allminitask.parceableFile.ParceableCthInt
import com.example.chapter_3_allminitask.serializable.Serializable

class topic4 : AppCompatActivity() {
    private lateinit var binding: ActivityTopic4Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTopic4Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAlarm.setOnClickListener {
            Call()
        }
        binding.toPercelableInt.setOnClickListener {
            val Intent = Intent(this, ParceableCthInt::class.java)
            startActivity(Intent)
        }
        binding.toPercelable.setOnClickListener {
            val intent = Intent(this, Parceable::class.java)
            startActivity(intent)
        }
        binding.toSerializable.setOnClickListener {
            val Intent = Intent(this, Serializable::class.java)
            startActivity(Intent)
        }
        binding.latihanParcelable.setOnClickListener {
            val Intent = Intent(this, Latihan::class.java)
            startActivity(Intent)
        }

    }

    private fun Call() {
        val Call = Intent().apply {
            action = Intent.ACTION_CALL
        }
        if (Call.resolveActivity(packageManager) == null) {
            startActivity(Call)
        }
    }


}