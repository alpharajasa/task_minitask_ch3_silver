package com.example.chapter_3_allminitask.latihan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivitySerializableLat2Binding
import com.example.chapter_3_allminitask.serializable.SerializableData

class SerializableLat2 : AppCompatActivity() {
    private lateinit var binding: ActivitySerializableLat2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySerializableLat2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val person = intent.getSerializableExtra("nani") as SerializableLatData

        binding.nama.text = person.nama
        binding.panggilan.text = person.panggilan
        binding.usia.text = person.usia
        binding.alamat.text = person.alamat
    }
}