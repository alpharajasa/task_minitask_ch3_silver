package com.example.chapter_3_allminitask.latihan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivityBundleLat2Binding

class BundleLat2 : AppCompatActivity() {

    private lateinit var binding : ActivityBundleLat2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBundleLat2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle = intent.extras
        val name = bundle?.getString("data1")
        val panggilan = bundle?.getString("data2")
        val usia = bundle?.getString("data3")
        val alamat = bundle?.getString("data4")
        binding.nama.text = name
        binding.panggilan.text = panggilan
        binding.usia.text = usia
        binding.alamat.text = alamat

    }
}