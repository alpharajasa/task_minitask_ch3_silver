package com.example.chapter_3_allminitask.latihan

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParcelableLatData(

    val nama: String,
    val panggilan: String,
    val usia: String,
    val alamat: String

) : Parcelable
