package com.example.chapter_3_allminitask.latihan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.chapter_3_allminitask.databinding.ActivityParcelableLatBinding

class Latihan : AppCompatActivity() {
    private lateinit var binding: ActivityParcelableLatBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParcelableLatBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.SendBundle.setOnClickListener {

            if (binding.namabang.text.isNotEmpty() && binding.alamatbang.text.isNotEmpty() && binding.usiabang.text.isNotEmpty() && binding.panggilanbang.text.isNotEmpty()) {


                val bundle = Bundle()

                bundle.putString("data1", binding.namabang.text.toString())
                bundle.putString("data2", binding.panggilanbang.text.toString())
                bundle.putString("data3", binding.usiabang.text.toString())
                bundle.putString("data4", binding.alamatbang.text.toString())

                val intent = Intent(this, BundleLat2::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Yg lengkap dong :)", Toast.LENGTH_LONG).show()
            }

        }


        binding.SendSerializable.setOnClickListener {

            if (binding.namabang.text.isNotEmpty() && binding.alamatbang.text.isNotEmpty() && binding.usiabang.text.isNotEmpty() && binding.panggilanbang.text.isNotEmpty()) {


                val nama = binding.namabang.text.toString()
                val panggilan = binding.panggilanbang.text.toString()
                val usia = binding.usiabang.text.toString()
                val alamat = binding.alamatbang.text.toString()

                val user = SerializableLatData(nama, panggilan, usia , alamat)

                val intent = Intent(this, SerializableLat2::class.java)
                intent.putExtra("nani", user)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Yg lengkap dong :)", Toast.LENGTH_LONG).show()
            }

        }

        binding.Sendcoy.setOnClickListener {

            if (binding.namabang.text.isNotEmpty() && binding.alamatbang.text.isNotEmpty() && binding.usiabang.text.isNotEmpty() && binding.panggilanbang.text.isNotEmpty()) {


                val nama = binding.namabang.text.toString()
                val panggilan = binding.panggilanbang.text.toString()
                val usia = binding.usiabang.text.toString()
                val alamat = binding.alamatbang.text.toString()


                val user = ParcelableLatData(nama, panggilan, usia , alamat)


                val intent = Intent(this, ParcelableLat2::class.java)
                intent.putExtra("user", user)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Yg lengkap dong :)", Toast.LENGTH_LONG).show()
            }

        }


    }
}