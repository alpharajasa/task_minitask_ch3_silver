package com.example.chapter_3_allminitask.latihan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivityParcelableLat2Binding

class ParcelableLat2 : AppCompatActivity() {
    private lateinit var binding: ActivityParcelableLat2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParcelableLat2Binding.inflate(layoutInflater)
        setContentView(binding.root)


        val user: ParcelableLatData = intent.getParcelableExtra("user")!!

        binding.nama.text = user.nama
        binding.panggilan.text = user.panggilan
        binding.usia.text = user.usia
        binding.alamat.text = user.alamat

    }
}