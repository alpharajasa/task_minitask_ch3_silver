package com.example.chapter_3_allminitask.latihan

import java.io.Serializable

data class SerializableLatData(

    val nama: String,
    val panggilan: String,
    val usia: String,
    val alamat: String

) : Serializable
