package com.example.chapter_3_allminitask.parceableFile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.chapter_3_allminitask.databinding.ActivityParceableCthIntBinding

class ParceableCthInt : AppCompatActivity() {
    private lateinit var binding: ActivityParceableCthIntBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParceableCthIntBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSend.setOnClickListener {

            if (binding.etName.text.isNotEmpty() && binding.etEmail.text.isNotEmpty() && binding.etUmur.text.isNotEmpty() ){


                    val name = binding.etName.text.toString()
                    val email = binding.etEmail.text.toString()
                    val umur = binding.etUmur.text.toString()

                    val user = ParceableCthIntUserID(name, email, umur)


                    val intent = Intent(this, ParcelableCthIntProfile::class.java)
                    intent.putExtra("user", user)
                    startActivity(intent)
                } else {
                Toast.makeText(this, "Yg lengkap dong :)", Toast.LENGTH_LONG).show()
            }

        }


    }
}

