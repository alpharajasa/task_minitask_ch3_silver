package com.example.chapter_3_allminitask.parceableFile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.R
import com.example.chapter_3_allminitask.databinding.ActivityParceable2Binding

class Parceable2 : AppCompatActivity() {
    private lateinit var binding : ActivityParceable2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParceable2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val mobil = intent.getParcelableExtra<ParceableData>("INI")
        if (mobil !=null){
            binding.textPiewParceable.text = "nama : ${mobil.namaMobil}\ntahun : ${mobil.tahun}\nwarna : ${mobil.warna}"
        }



    }
}