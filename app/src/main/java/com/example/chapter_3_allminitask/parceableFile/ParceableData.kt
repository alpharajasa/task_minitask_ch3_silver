package com.example.chapter_3_allminitask.parceableFile

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParceableData(

    val namaMobil : String,
    var tahun : Int,
    var warna : String

) : Parcelable