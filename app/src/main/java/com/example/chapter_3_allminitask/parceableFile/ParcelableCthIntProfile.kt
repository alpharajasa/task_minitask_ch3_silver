package com.example.chapter_3_allminitask.parceableFile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.chapter_3_allminitask.databinding.ActivityParcelableCthIntProfileBinding


class ParcelableCthIntProfile : AppCompatActivity() {

    private lateinit var binding: ActivityParcelableCthIntProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParcelableCthIntProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val user: ParceableCthIntUserID = intent.getParcelableExtra("user")!!

        binding.tvName.text = user.name
        binding.tvEmail.text = user.email
        binding.tvUmur.text = user.umur

        val umuran = (binding.tvUmur.text as String).toInt()

        if (umuran % 2 == 0) {
            binding.tvGG.text = "Genap"
        } else {
            binding.tvGG.text = "Ganjil"
        }

    }
}