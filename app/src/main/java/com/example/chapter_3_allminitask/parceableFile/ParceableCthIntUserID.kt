package com.example.chapter_3_allminitask.parceableFile

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class ParceableCthIntUserID(

    val name: String,
    val email: String,
    val umur: String
) : Parcelable