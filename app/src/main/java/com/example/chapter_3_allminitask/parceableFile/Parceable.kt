package com.example.chapter_3_allminitask.parceableFile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivityParceableBinding

class Parceable : AppCompatActivity() {
    private lateinit var binding: ActivityParceableBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParceableBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.butonParceable.setOnClickListener {
            val intent = Intent(this, Parceable2::class.java)
            val mobil = ParceableData("Ferarri", 1970, "Pink")
            intent.putExtra("INI", mobil)
            startActivity(intent)
        }


    }
}