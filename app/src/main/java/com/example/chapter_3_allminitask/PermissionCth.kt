package com.example.chapter_3_allminitask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.chapter_3_allminitask.databinding.ActivityLifecyleBinding
import com.example.chapter_3_allminitask.databinding.ActivityPermissionCthBinding

class PermissionCth : AppCompatActivity() {
    private lateinit var binding: ActivityPermissionCthBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPermissionCthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLoadImage.setOnClickListener {
            Glide.with(this)
                .load("https://i.ibb.co/zJHYGBP/binarlogo.jpg")
                .circleCrop()
                .into(binding.ivBinar)
        }

    }
}