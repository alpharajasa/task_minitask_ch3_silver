package com.example.chapter_3_allminitask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.chapter_3_allminitask.databinding.ActivityLifecyleBinding

class ActivityLifecyle : AppCompatActivity() {

    private lateinit var binding: ActivityLifecyleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLifecyleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Toast.makeText(this, "On create", Toast.LENGTH_SHORT).show()
        Log.d("tes", "onCreate")

    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "OnStart", Toast.LENGTH_SHORT).show()
        Log.d("tes", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "OnResume", Toast.LENGTH_SHORT).show()
        Log.d("tes", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "OnPause", Toast.LENGTH_SHORT).show()
        Log.d("tes", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "OnStop", Toast.LENGTH_SHORT).show()
        Log.d("tes", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_SHORT).show()
        Log.d("tes", "onDestroy")
    }

}