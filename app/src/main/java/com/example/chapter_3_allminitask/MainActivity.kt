package com.example.chapter_3_allminitask

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.chapter_3_allminitask.databinding.ActivityMainBinding
import com.example.chapter_3_allminitask.fragment.iniActivityFragment
import com.example.chapter_3_allminitask.navComp.NavComp

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.topic1.setOnClickListener {
            val Intent = Intent(this, PermissionCth::class.java)
            startActivity(Intent)
        }
        binding.topic1.setOnLongClickListener {
            Toast.makeText(this, "Android Permission", Toast.LENGTH_LONG).show()
            true
        }
        binding.topic2.setOnClickListener {
            val Intent = Intent(this, ActivityLifecyle::class.java)
            startActivity(Intent)
        }
        binding.topic2.setOnLongClickListener {
            Toast.makeText(this, "Activity Lifecycle", Toast.LENGTH_LONG).show()
            true
        }
        binding.topic3.setOnClickListener {
            val Intent = Intent(this, iniActivityFragment::class.java)
            startActivity(Intent)
        }
        binding.topic3.setOnLongClickListener {
            Toast.makeText(this, "Activity Fragment", Toast.LENGTH_LONG).show()
            true
        }
        binding.topic4.setOnClickListener {
            val Intent = Intent(this, topic4::class.java)
            startActivity(Intent)
        }
        binding.topic4.setOnLongClickListener {
            Toast.makeText(this, "Intent, Bundle, Seriazble, Percelable", Toast.LENGTH_LONG).show()
            true
        }
        binding.topic5.setOnClickListener {
            val Intent = Intent(this, NavComp::class.java)
            startActivity(Intent)
        }
        binding.topic5.setOnLongClickListener {
            Toast.makeText(this, "Navigation Component", Toast.LENGTH_LONG).show()
            true
        }

    }
}