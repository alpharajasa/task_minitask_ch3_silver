package com.example.chapter_3_allminitask.serializable

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivitySerializable2Binding

class Serializable2 : AppCompatActivity() {
    private lateinit var binding: ActivitySerializable2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySerializable2Binding.inflate(layoutInflater)
        setContentView(binding.root)


        // Seriazable
        val person = intent.getSerializableExtra("AN_OBJECT") as SerializableData
        if (person != null) {
            binding.initext.text = "Hi ${person.nama}\nnim : ${person.nim}\numur : ${person.umur}"
        }


    }
}