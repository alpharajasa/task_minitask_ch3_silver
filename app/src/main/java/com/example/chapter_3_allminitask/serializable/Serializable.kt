package com.example.chapter_3_allminitask.serializable

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chapter_3_allminitask.databinding.ActivitySerializableBinding

open class Serializable : AppCompatActivity() {
    private lateinit var binding: ActivitySerializableBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySerializableBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.butonSeriazable.setOnClickListener {
            val intent = Intent(this, Serializable2::class.java)
            val person = SerializableData("kang cilok", 2020202020, 12)
            intent.putExtra("AN_OBJECT", person)
            startActivity(intent)

        }
    }
}