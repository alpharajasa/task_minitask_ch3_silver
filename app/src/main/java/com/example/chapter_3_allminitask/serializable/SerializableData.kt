package com.example.chapter_3_allminitask.serializable

import java.io.Serializable

data class SerializableData(

    var nama : String,
    var nim : Int,
    var umur : Int

): Serializable
